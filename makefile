pkg := oldstandard
SHELL = bash
ver  := $(shell grep '\\edef\\pkgver' $(pkg).tex | grep -Eo '[0-9]{1,}\.[0-9]{1,}[a-z]{,1}')
TEXMFDIR := $(shell kpsewhich -expand-var='$$TEXMFHOME')
HOMEDIR := $$HOME
LOCALFONTDIR := $(HOMEDIR)/.local/share/fonts

doc: local
	@echo "Building the documentation. Please wait..."
	lualatex --shell-escape $(pkg).tex >/dev/null
	biber $(pkg) >/dev/null
	lualatex --shell-escape $(pkg).tex >/dev/null
	lualatex --shell-escape $(pkg).tex >/dev/null
	@echo "Building font table..."
	lualatex --shell-escape fonttable.tex >/dev/null
	lualatex --shell-escape fonttable.tex >/dev/null
	@echo "Removing OldStandard .otf files from $(LOCALFONTDIR)..."
	rm -f $(LOCALFONTDIR)/OldStandard-*.otf
	luaotfload-tool --update
	@echo "Done."

all: doc

local:
	@rm -f $(LOCALFONTDIR)/OldStandard-*.otf
	@echo "Installing latest OldStandard .otf files in $(LOCALFONTDIR)..."
	cp otf/OldStandard-*.otf $(LOCALFONTDIR)/
	luaotfload-tool --update
	@echo "Installing .fontspec files..."
	if [ ! -d "$(TEXMFDIR)/tex/latex/fontspec" ]; then \
	mkdir -p $(TEXMFDIR)/tex/latex/fontspec; \
	fi
	cp fontspec/OldStandard.fontspec $(TEXMFDIR)/tex/latex/fontspec
	@echo "Done."

package: doc
	mkdir $(pkg)-$(ver)
	cp *.{txt,md,lua,tex,pdf} $(pkg)-$(ver)
	cp otf/*.otf $(pkg)-$(ver)
	cp fontspec/*.fontspec $(pkg)-$(ver)
	tar czf $(pkg)-$(ver).tar.gz $(pkg)-$(ver)
	@echo "$(pkg)-$(ver).tar.gz has been created."

clean:
	@echo "Removing OldStandard .otf files from $(LOCALFONTDIR)..."
	rm -f $(LOCALFONTDIR)/OldStandard-*.otf
	luaotfload-tool --update
	@echo "Removing .fontspec files from local texmf dir..."
	rm -f $(TEXMFDIR)/tex/latex/fontspec/OldStandard.fontspec
	git clean -df
	pandoc README.md -o about.html
	@echo "Done."

.PHONY: doc all local package clean
